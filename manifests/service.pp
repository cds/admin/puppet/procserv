define procserv::service (
    Integer $logport,
    Integer $controlport,
    String $logfile,
    String $script,
    Optional[String] $user = undef,
    Optional[String] $group = undef,
    Boolean $service_enable = true,
    Optional[Hash[String, String]] $extra_vars = {},
    Enum['running', 'stopped'] $service_ensure = 'running',
) {
    include ::procserv

    if $user {
	$logfileUser = "${user}"
    } else {
	$logfileUser = "root"
    }
    if $group {
	$logfileGroup = "${group}"
    } else {
	$logfileGroup = "root"
    }

    file {"${logfile}":
        ensure => 'file',
	owner => "${logfileUser}",
	group => "${logfileGroup}",
	mode => '0644',
    }

    file {"/etc/sysconfig/procserv-${title}":
	ensure => 'file',
        owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('procserv/sysconfig.epp', {
	    logport => $logport,
	    logfile => $logfile,
	    controlport => $controlport,
	    script => $script,
	    extra_vars => $extra_vars,
	}),
	notify => Service["procserv@${title}"],
	require => File["${logfile}"],
    }

    file {"/etc/logrotate.d/${title}":
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('procserv/logrotate.epp', {
            logfile => $logfile,
	    command => $script,
        }),
        require => File["${logfile}"],
    }

    service {"procserv@${title}":
        enable => $service_enable,
        ensure => $service_ensure,
        require => Package['procServ-systemd'],
    }

    if $user or $group {
	file {"/etc/systemd/system/procserv@${title}.service.d":
	    ensure => 'directory',
	    owner => 'root',
	    group => 'root',
	    mode => '0755',
	}

	file {"/etc/systemd/system/procserv@${title}.service.d/user.conf":
	    ensure => 'file',
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('procserv/user.conf.epp', {
		user => $user,
		group => $group,
	    }),
	    notify => [Exec['systemd-reload-procserv'], Service["procserv@${title}"]],
	}
    } else {
	file {"/etc/systemd/system/procserv@${title}.service.d/user.conf":
	    ensure => 'absent',
	    notify => [Exec['systemd-reload-procserv'], Service["procserv@${title}"]],
	}
    }
}
