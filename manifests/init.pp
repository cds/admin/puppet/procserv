class procserv (
    String $package_ensure,
    Hash[String, Struct[{logport => Integer,
                         controlport => Integer,
			 script => String,
			 logfile => String,
			 Optional[extra_vars] => Hash[String,String]}]] $services,
) {
    package {'procServ':
        ensure => $package_ensure,
    }

    package {'procServ-systemd':
        ensure => 'installed',
    }

    exec {'systemd-reload-procserv':
        command => '/bin/systemctl daemon-reload',
	refreshonly => true,
    } -> Service<||>

    $procserv::services.each |String $name, Hash $service_hash| {
	procserv::service {"${name}":
	    logport => $service_hash['logport'],
	    controlport => $service_hash['controlport'],
	    logfile => $service_hash['logfile'],
	    script => $service_hash['script'],
	    extra_vars => $service_hash['extra_vars'],
	}
    }
}
